var g = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
//var sh = require('shelljs');

var $dest = 'www/';
var $tmp = 'src/tmp/';
var $src = {
    js: [
        "bower_components/ionic/js/ionic.bundle.min.js",
        "bower_components/sails.io.js/dist/sails.io.js",
        "bower_components/angular-sails/dist/angular-sails.min.js",
        "src/tmp/app.js",
        "src/tmp/tpl.js"
    ],
    app: [
        'src/app.js',
        'src/App.js',
        'src/index.js',
        'src/*.js',
        '!src/_*.js',
        '!src/*_.js'
    ],
    tpl: [
        'src/*.html',
        '!src/index.html',
        '!src/_*.html',
        '!src/*_.html'
    ],
    css: 'src/app.scss',
    assets: [
        'src/index.html',
        'bower_components/ionic/fonts/*',
        'src/img/*'
    ],
    pack: [
        '**',
        '!bower_components/**',
        '!node_modules/**',
        '!src/**',
        '!.git/**',
        '!.gitignore',
        '!gulpfile.js',
        '!bower.json',
        '!package.json',
        '!README.md'
    ],
    rm: $dest + '**/*.+(html|js|css)'
};









g.task('default', ['serve', 'w-js', 'w-css', 'w-assets']);

g.task('serve', function() {
    browserSync({
        server: {
            baseDir: $dest,
            //directory: true
        },
        port: 8100
    });

    g.watch($dest + '**/*.+(html|js|css)', browserSync.reload);
});

g.task('rm', function() {
    return g.src($src.rm, {
            read: false
        })
        .pipe($.rimraf());
});

g.task('w-js', ['js'], function() {
    g.watch($src.app.concat($src.tpl), ['js']);
});

g.task('w-css', ['css'], function() {
    g.watch($src.css, {
        interval: 3000,
        debounceDelay: 1000,
        //mode: 'auto' //'auto','watch' (force native events), or 'poll' (force stat polling)
    }, ['css']);
});

g.task('w-assets', ['assets'], function() {
    g.watch($src.assets, ['assets']);
});

g.task('pack', ['js', 'css', 'assets'], function() {
    var date = new Date();
    return g.src($src.pack, {
            dot: true
        })
        .pipe($.zip('app.' + date.toUTCString() + '.zip'))
        .pipe(g.dest('../'));
});









g.task('jshint', function() {
    return g.src($src.app)
        .pipe($.jshint())
        .pipe($.jshint.reporter('default'));
});

g.task('app', ['jshint'], function() {
    return g.src($src.app)
        .pipe($.concat('app.js'))
        .pipe($.ngAnnotate({
            remove: true,
            add: true,
        }))
        .pipe($.changed($tmp, {
            hasChanged: $.changed.compareSha1Digest
        }))
        .pipe(g.dest($tmp));
});

g.task('tpl', function() {
    return g.src($src.tpl)
        .pipe($.angularTemplatecache({
            filename: 'tpl.js',
            module: 'app'
        }))
        .pipe($.changed($tmp, {
            hasChanged: $.changed.compareSha1Digest
        }))
        .pipe(g.dest($tmp));
});

g.task('js', ['app', 'tpl'], function() {
    return g.src($src.js)
        .pipe($.concat('app.js'))
        //.pipe($.uglify())
        .pipe($.changed($dest, {
            hasChanged: $.changed.compareSha1Digest
        }))
        .pipe(g.dest($dest));
});

g.task('css', function() {
    return g.src($src.css)
        .pipe($.sass())
        .pipe(g.dest($tmp))
        //.pipe($.minifyCss({
        //    keepSpecialComments: 0
        //}))
        .pipe($.changed($dest, {
            hasChanged: $.changed.compareSha1Digest
        }))
        .pipe(g.dest($dest));
});

g.task('assets', function() {
    return g.src($src.assets)
        .pipe($.changed($dest))
        .pipe(g.dest($dest));
});
