angular.module('app')

.controller("sails", function($scope, $sails) {
    $scope.bars = [];

    (function() {
        $sails.get("/bars")
            .success(function(data) {
                $scope.bars = data;
            })
            .error(function(data) {
                alert('Houston, we got a problem!');
            });


        $sails.on("message", function(message) {
            if (message.verb === "create") {
                $scope.bars.push(message.data);
            }
        });
    }());
});
