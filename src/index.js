angular.module('app')

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    /*.state('app', {
        abstract: true,
        views: {
            '@': {
                templateUrl: 'menus.html',
            }
        }
    })*/

    .state('menus', {
        abstract: true,
        views: {
            '': {
                templateUrl: 'menus.html',
                controller: 'root'
            },
            'left@menus': {
                templateUrl: 'menus-left.html',
            },
            'right@menus': {
                templateUrl: 'menus-right.html',
            }
        },
    })

    .state('tabs', {
        parent: 'menus',
        abstract: true,
        url: '/tabs',
        views: {
            '': {
                templateUrl: 'tabs.html',
            }
        }
    })

    .state('tabs.home', {
        url: '/home',
        views: {
            '': {
                templateUrl: 'tabs.home.html'
            }
        }
    })

    .state('tabs.fav', {
        url: '/fav',
        views: {
            'fav': {
                templateUrl: 'tabs.fav.html'
            }
        }
    })

    .state('tabs.chat', {
        url: '/chat',
        views: {
            'chat': {
                templateUrl: 'tabs.chat.html'
            }
        }
    })

    .state('tabs.pub', {
        url: '/pub',
        views: {
            'pub': {
                templateUrl: 'tabs.pub.html'
            }
        }
    })

    .state('tabs.set', {
        url: '/set',
        views: {
            'set': {
                templateUrl: 'tabs.set.html'
            }
        }
    })

    .state('tabs.playlists', {
        url: '/playlists',
        views: {
            'fav': {
                templateUrl: 'app.playlists.html',
                controller: 'PlaylistsCtrl'
            }
        }
    })

    .state('tabs.playlist', {
        url: '/playlists/:playlistId',
        views: {
            'fav': {
                templateUrl: 'app.playlists.playlist.html',
                controller: 'PlaylistCtrl'
            }
        }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/tabs/home');
});
