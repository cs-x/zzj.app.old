// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'ngSails'])

.constant('$server', 'http://toili.com')

/*
.constant('$ionicNavBarConfig', {
    transition: 'nav-title-slide-ios7',
    alignTitle: 'center',
    backButtonIcon: 'ion-ios7-arrow-back'
})

.constant('$ionicNavViewConfig', {
    transition: 'slide-left-right-ios7'
})

.constant('$ionicTabsConfig', {
    type: '',
    position: ''
})
*/

.config(function($ionicPlatformDefaults, $injector) {
    $ionicPlatformDefaults.android = $ionicPlatformDefaults.ios;
    angular.forEach($ionicPlatformDefaults.ios, function(defaults, constantName) {
        angular.extend($injector.get(constantName), defaults);
    });
})

.config(function($sailsProvider, $server) {
    $sailsProvider.url = $server;
})

.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault(); //StatusBar.hide();
        }
    });
});

/*
IonicModule
.constant('$ionicPlatformDefaults', {
  'ios': {
    '$ionicNavBarConfig': {
      transition: 'nav-title-slide-ios7',
      alignTitle: 'center',
      backButtonIcon: 'ion-ios7-arrow-back'
    },
    '$ionicNavViewConfig': {
      transition: 'slide-left-right-ios7'
    },
    '$ionicTabsConfig': {
      type: '',
      position: ''
    }
  },
  'android': {
    '$ionicNavBarConfig': {
      transition: 'nav-title-slide-ios7',
      alignTitle: 'center',
      backButtonIcon: 'ion-ios7-arrow-back'
    },
    '$ionicNavViewConfig': {
      transition: 'slide-left-right-ios7'
    },
    '$ionicTabsConfig': {
      type: 'tabs-striped',
      position: ''
    }
  }
});
*/
